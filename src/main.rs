#[macro_use]
extern crate prettytable;

use prettytable::{Cell, Row, Table};
use std::collections::HashMap;
use std::iter::FromIterator;
use std::io;
use std::io::Write;

#[derive(Debug, Clone)]
struct Contact {
    name: String,
    phone: String, // Consider only Indian for now
}

#[derive(Debug)]
struct Contacts {
    people: Vec<Contact>,
}

enum ContactError<'a> {
    PhoneNotFound(&'a str),
    NameNotFound(&'a str),
    DuplicatePhone(Contact),
}

impl Contacts {
    fn add(&mut self, name: String, phone: String) -> Result<(), ContactError> {
        match self.people.iter().find(|&person| person.phone == phone) {
            Some(person) => return Err(ContactError::DuplicatePhone(person.clone())),
            None => (),
        }
        self.people.push(Contact {
            name: name,
            phone: phone,
        });
        Ok(())
    }
    fn remove(&mut self, index: usize) {
        self.people.remove(index);
    }
}

type InputValidator = fn(&String) -> bool;

fn no_validation(input: &String) -> bool {
    return true;
}

fn get_input(prompt: &str, trim: bool, validator: Option<InputValidator>) -> String {
    let mut value = String::new();
    loop {
        print!("{}", prompt);
        std::io::stdout().flush().expect("Unable to flush stdout");
        io::stdin()
            .read_line(&mut value)
            .expect("Unable to read line");
        if trim {
            value = value.trim().to_string();
        }
        if validator.unwrap_or(no_validation)(&value) {
            return value;
        }
    }
}

fn add_contact(contacts: &mut Contacts) {
    println!("Adding contact details interactively");
    let name = get_input("Name: ", true, None);
    let phone = get_input("Phone: ", true, None);
    match contacts.add(name, phone) {
        Ok(()) => println!("Done"),
        Err(error) => match error {
            ContactError::DuplicatePhone(person) => {
                println!("Phone already exists for '{}'", person.name)
            }
            _ => println!("Unkown error"),
        },
    }
}

fn raw(contacts: &mut Contacts) {
    println!("Contacts: {:?}", contacts);
}

fn find_contact(contacts: &Contacts, string: &String) -> Vec<usize> {
    let string = string.to_lowercase();
    let words: Vec<&str> = string.split(' ').collect();
    let mut all_matches = Vec::new();
    let mut word_matches = Vec::new();
    let mut substr_matches = Vec::new();
    for (i, person) in contacts.people.iter().enumerate() {
        let mut flag = false;
        let phone = person.phone.to_lowercase();
        let name = person.name.to_lowercase();
        if phone == string || name == string {
            all_matches.push(i);
            flag = true;
        }
        if !flag {
            for word in words.iter() {
                if phone.contains(word) || name.contains(word) {
                    word_matches.push(i);
                    flag = true;
                    break;
                }
            }
        }
        if !flag && (phone.contains(&string) || name.contains(&string)) {
            substr_matches.push(i);
        }
    }
    all_matches.append(&mut word_matches);
    all_matches.append(&mut substr_matches);
    return all_matches;
}

fn remove_contact(contacts: &mut Contacts) {
    println!("Removing contact by");
    let string = get_input("Enter string (phone/name): ", true, None);
    let all_matches = find_contact(contacts, &string);
    for (i, index) in all_matches.iter().enumerate() {
        println!("{}. {:?}", i + 1, contacts.people[*index]);
    }
    let to_delete = get_input("Enter choice(s): ", true, None);
}

fn exit(_contacts: &mut Contacts) {
    // TODO: Take some action for saving contacts before exit. Ask user.
    std::process::exit(0);
}

fn main() {
    let mut contacts = Contacts { people: Vec::new() };
    let h: HashMap<&'static str, fn(&mut Contacts)> = HashMap::from_iter([
        ("exit", exit as fn(&mut Contacts)),
        ("raw", raw),
        ("quit", exit),
        ("add", add_contact),
        ("remove", remove_contact),
    ]);
    loop {
        let value = get_input("\n\x1b[01;32mcmd>\x1b[0m ", true, None);
        match h.get(value.as_str()) {
            Some(value) => value(&mut contacts),
            None => {
                if (!value.is_empty()) {
                    println!("Invalid command '{}'", value);
                }
            }
        }
    }
}
